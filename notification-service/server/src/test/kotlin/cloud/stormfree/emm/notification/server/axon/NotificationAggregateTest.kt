package cloud.stormfree.emm.notification.server.axon

import cloud.stormfree.emm.notification.server.aggregate.NotificationAggregate
import cloud.stormfree.emm.notification.server.command.CreateNotificationCommand
import cloud.stormfree.emm.notification.server.event.CreateNotificationEvent
import org.axonframework.messaging.interceptors.BeanValidationInterceptor
import org.axonframework.test.aggregate.AggregateTestFixture
import org.axonframework.test.aggregate.FixtureConfiguration
import org.junit.Before
import org.junit.Test
import java.util.*

class NotificationAggregateTest {

    private lateinit var fixture: FixtureConfiguration<NotificationAggregate>


    @Before
    fun setUp() {
        fixture = AggregateTestFixture<NotificationAggregate>(NotificationAggregate::class.java)

    }

    @Test
    fun createNotificationTest() {
        val command = CreateNotificationCommand("abc")
        val event = CreateNotificationEvent("abc")
        fixture.given().`when`(command).expectEvents(event)
    }

}