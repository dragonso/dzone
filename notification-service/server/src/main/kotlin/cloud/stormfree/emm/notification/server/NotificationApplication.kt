package cloud.stormfree.emm.notification.server

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.cloud.client.discovery.EnableDiscoveryClient
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.mongodb.core.mapping.event.ValidatingMongoEventListener
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean
import javax.validation.Validator


@Configuration
@SpringBootApplication
@EnableConfigurationProperties
class NotificationApplication {}

fun main(args: Array<String>) {
    SpringApplication.run(NotificationApplication::class.java, *args)
}