package cloud.stormfree.emm.notification.server.configuration

import com.mongodb.MongoClient
import org.axonframework.commandhandling.CommandBus
import org.axonframework.eventsourcing.EventCountSnapshotTriggerDefinition
import org.axonframework.eventsourcing.Snapshotter
import org.axonframework.eventsourcing.eventstore.EmbeddedEventStore
import org.axonframework.eventsourcing.eventstore.EventStorageEngine
import org.axonframework.messaging.interceptors.BeanValidationInterceptor
import org.axonframework.mongo.DefaultMongoTemplate
import org.axonframework.mongo.eventsourcing.eventstore.MongoEventStorageEngine
import org.axonframework.spring.config.AxonConfiguration
import org.axonframework.spring.eventsourcing.SpringAggregateSnapshotterFactoryBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

/**
 * Contains several beans for configuration related to Axon
 */
@Configuration
open class AxonConfiguration {

    @Autowired
    open fun registerInterceptors(commandBus: CommandBus) {
        commandBus.registerDispatchInterceptor(BeanValidationInterceptor())
    }

    @Bean
    open fun snapshotterFactoryBean() = SpringAggregateSnapshotterFactoryBean()

    @Bean
    open fun eventStore(storageEngine: EventStorageEngine, configuration: AxonConfiguration): EmbeddedEventStore {
        return EmbeddedEventStore(storageEngine)
    }

    /**
     * Declares that events should be stored in MongoDB
     */
    @Bean
    open fun storageEngine(client: MongoClient): EventStorageEngine {
        return MongoEventStorageEngine(DefaultMongoTemplate(client))
    }


}
