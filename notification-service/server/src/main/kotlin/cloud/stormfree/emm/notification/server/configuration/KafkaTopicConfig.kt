package cloud.stormfree.emm.notification.server.configuration

import org.apache.kafka.clients.admin.AdminClientConfig
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.kafka.core.KafkaAdmin


/**
 * Configuration for kafka topics
 */
@Configuration
@Profile("production")
open class KafkaTopicConfig {

    @Value(value = "\${kafka.bootstrap.servers}")
    lateinit var bootstrapAddress: String

    /**
     * Creates a [KafkaAdmin] that handles topic creation
     */
    @Bean
    open fun kafkaAdmin(): KafkaAdmin {
        val configs: MutableMap<String, Any> = mutableMapOf<String, Any>()
        configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress)
        return KafkaAdmin(configs)
    }

    companion object {
        /**
         * Constant so that the kafka topic can be maintained in a single place
         */
        const val KAFKA_TOPIC = "events.notification"
    }
}