package cloud.stormfree.emm.notification.server.handler


import cloud.stormfree.emm.notification.server.event.CreateNotificationEvent
import org.axonframework.config.ProcessingGroup
import org.axonframework.eventhandling.AllowReplay
import org.axonframework.eventhandling.EventHandler
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component



@Component
@ProcessingGroup("notification")
open class NotificationHandler constructor() {

    val log = LoggerFactory.getLogger(NotificationHandler::class.java)

    /**
     * Creates a new account and then emits it to be picked up by queries
     */
    @EventHandler
    @AllowReplay(false)
    open fun create(event: CreateNotificationEvent) {
        log.info("notfied");

    }


}