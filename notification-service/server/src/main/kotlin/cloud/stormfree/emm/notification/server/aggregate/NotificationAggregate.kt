package cloud.stormfree.emm.notification.server.aggregate


import cloud.stormfree.emm.notification.server.command.CreateNotificationCommand
import cloud.stormfree.emm.notification.server.event.CreateNotificationEvent
import org.axonframework.commandhandling.CommandHandler
import org.axonframework.commandhandling.model.AggregateIdentifier
import org.axonframework.commandhandling.model.AggregateLifecycle
import org.axonframework.commandhandling.model.AggregateLifecycle.apply
import org.axonframework.commandhandling.model.AggregateLifecycle.markDeleted
import org.axonframework.eventsourcing.EventSourcingHandler
import org.axonframework.spring.stereotype.Aggregate

/**
 * Axon Aggregate representing a single notification and its state changes
 */
@Aggregate()
class NotificationAggregate {

    /**
     * Unique id for an aggregate.
     */
    @AggregateIdentifier
    private lateinit var id: String

    private var notified = false;

    /**
     * Empty constructor required for Axon
     */
    constructor()

    /**
     * Handles the [CreateNotificationCommand]. Constructs a new instance of an account aggregate and then creates a [CreateNotificationEvent] and sends it to any @EventHandler or @EventSourcingHandler
     * @param command Command to be handled
     */
    @CommandHandler
    constructor(command: CreateNotificationCommand) {
        AggregateLifecycle.apply(CreateNotificationEvent(command.targetAggregateIdentifier))
    }


    @EventSourcingHandler
    fun on(event: CreateNotificationEvent) {
        id = event.aggregateIdentifier

    }


}