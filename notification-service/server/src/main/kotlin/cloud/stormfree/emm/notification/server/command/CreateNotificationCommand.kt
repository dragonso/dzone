package cloud.stormfree.emm.notification.server.command

import org.axonframework.commandhandling.TargetAggregateIdentifier



data class CreateNotificationCommand(@TargetAggregateIdentifier val targetAggregateIdentifier: String)