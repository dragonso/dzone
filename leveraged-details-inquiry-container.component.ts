import { Component, ElementRef, EventEmitter, OnInit, Output, TemplateRef, ViewChild, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { selectErrors, selectErrorStatus, selectRequestingStatus } from '../../core/store/app.selectors';
import { selectActiveAgreementPortfolioCollateralItems } from '../../core/store/collateral-items/collateral-items.selectors';
import { CollateralItems } from '../../core/store/collateral-items/collateral-items.model';
import { expand } from '../../shared/animations';
import { heightSlide } from '../../shared/loader/loader.animation';
import { StatusModel } from '../../core/store/common/status/status.model';
import * as LeverageDetailsActions from '../store/leveraged-details.actions';
import { COLLAPSED, EXPANDED } from '../../shared/animations.states';
import { LeveragedDetailFormModel } from '../store/leveraged-details.model';
import {
  selectShowAddCollateralItemForm,
  selectLeveragedDetails, showNewCollateralItemAddedMessage
} from '../store/leveraged-details.selectors';
import { MatDialog, MatDialogRef } from '@angular/material';
import * as CollateralActions from 'app/core/store/collateral-items/collateral-items.actions';
import { selectAgreementPortfolioId } from 'app/core/store/agreement-portfolio/agreement-portfolio.selectors';
import {Subscription } from '../../../../node_modules/rxjs/Subscription';


@Component({
  selector: 'rip-leveraged-details-inquiry-container',
  templateUrl: './leveraged-details-inquiry-container.component.html',
  styleUrls: ['./leveraged-details-inquiry-container.component.css'],
  animations: [
    expand,
    heightSlide
  ]
})
export class LeveragedDetailsInquiryContainerComponent implements OnInit, OnDestroy {

  readonly expand: string = EXPANDED;
  readonly collapse: string = COLLAPSED;
  isAddLeveragedExpanded: Observable<boolean>;
  leveragedDetailModel: Observable<LeveragedDetailFormModel>;
  isRequesting: Observable<boolean>;
  sessionData: Observable<string>;
  isErrorStatus: Observable<boolean>;
  errors: Observable<Map<string, StatusModel>>;
  agreementPortfolioCollateralItemsSlice: Observable<CollateralItems[]>;
  readonly READ_SLICE = ['agreementPortfolioCollateralItems.read', 'agreementPortfolioCollateralItems.update'];
  showNewAddLeveraged: Observable<boolean>;
  @ViewChild('statusLeveragedContainer') statusMessagesDiv: ElementRef;
  @Output() onRenderComplete = new EventEmitter<boolean>();
  private detailIdToRemove: string;
  dialogRef: MatDialogRef<any, any>;
  @ViewChild('deleteLeverageDetailModal') modalTemplate: TemplateRef<any>;
  @ViewChild('addMessageDiv') newLeverageDetailDiv: ElementRef;
  private agreementId: string;
  private leverageDetailToRemove: CollateralItems;
  private subs = new Subscription();


  constructor(public translateServ: TranslateService, public store: Store<any>,
              private dialogService: MatDialog) { }

  ngOnInit() {
    this.isRequesting = this.store.select(selectRequestingStatus(this.READ_SLICE));
    this.isErrorStatus = this.store.select(selectErrorStatus(this.READ_SLICE));
    this.errors = this.store.select(selectErrors(this.READ_SLICE));
    this.isAddLeveragedExpanded = this.store.select(selectShowAddCollateralItemForm());
    this.leveragedDetailModel = this.store.select(selectLeveragedDetails());
    /* this slice selection is required here, so as to determine whether the account is leveraged or not, empty collaterItems[] denote account not leveraged */
    this.agreementPortfolioCollateralItemsSlice = this.store.select(selectActiveAgreementPortfolioCollateralItems());
    this.showNewAddLeveraged = this.store.select(showNewCollateralItemAddedMessage());
    this.store.dispatch(
      new LeverageDetailsActions.ClearSuccessAddLeveragedDetail()
    );

    const sub1 =  this.showNewAddLeveraged.subscribe((value) => {
      if (value) {
        this.scrollToContent(this.statusMessagesDiv);
      }
    });

    this.store.select(selectAgreementPortfolioId).subscribe(val => this.agreementId = val).unsubscribe();

    const sub2 =  this.isAddLeveragedExpanded.subscribe((value)=>{
      if(value){
        this.scrollToContent(this.newLeverageDetailDiv);
      }    
    });

    this.subs.add(sub1);
    this.subs.add(sub2);
  }

  handleAddLeveragedDetail() {
    this.store.dispatch(
      new LeverageDetailsActions.ShowAddLeveragedDetail()
    );
  }

  private scrollToContent(element: ElementRef): void {
    setTimeout(() => {
      
      element.nativeElement.scrollIntoView({
        behavior: 'smooth',
        block: 'start',
        inline: 'nearest'
      });
    }, 1000);
  }

  removeLeverageDetail(detailId: string) {
    this.detailIdToRemove = detailId;
    this.dialogRef = this.dialogService.open(this.modalTemplate);
  }

  confirmDelete() {
    this.agreementPortfolioCollateralItemsSlice.subscribe((value: CollateralItems) => {
      this.leverageDetailToRemove = value.find( item => item.agreement.itemId === this.detailIdToRemove );
    }).unsubscribe();

    if (this.leverageDetailToRemove) {
      this.store.dispatch(
        new CollateralActions.RequestUpdateAgreementPortfolioCollateralItems({
          planId: this.agreementId,
          collateralItem: {
            agreement: {...this.leverageDetailToRemove.agreement, statusCd: 'inactive'}
          }
        })
      );
    }

    this.dialogRef.close();
  }

  cancel() {
    this.dialogRef.close();
  }

  ngOnDestroy(){
    this.subs.unsubscribe();
  }
}
